# Protractor for Non Angular WebApps

This is an example on how to implement Protractor tests for non angular web applications.

Works with Selenium 3.141.0

## Test Frameworks
This example has two implementations with two different test frameworks: Mocha and Cucumber.
In both cases, it is expected to have an already Selenium server running at `localhost:4444`.

If you are not using your own Selenium server implementation you can run `npm run startSelenium` before to have `webdriver-manager` doing it for you.

### Mocha

Runs default Protractor configuration file: `protractor.conf.js`

NPM command: `npm test`

### Cucumber

Runs default Cucumber/Protractor configuration file: `cucumber.conf.js`

NPM command: `npm run cucumber`
