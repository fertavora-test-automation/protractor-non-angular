exports.config = {
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  cucumberOpts: {
    require: ['./cucumber/features/steps/*.js'],
    format: [require.resolve('cucumber-pretty')]
  },
  capabilities: {
    browserName: 'chrome',
    'goog:chromeOptions': {
      args: ['start-maximized']
    }
  },
  specs: [
    'cucumber/features/saucedemo.feature'
  ],
  baseUrl: 'https://www.saucedemo.com',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  SELENIUM_PROMISE_MANAGER: false,
  onPrepare: () => browser.waitForAngularEnabled(false)
};
