const { productsPage, productDetailsPage } = require('../../../pageobjects');
const { When } = require('cucumber');
const chai = require('chai');
const { expect } = chai;

When(/the user selects product (.*)/, async function (product) {
  await productsPage.clickItemByLabel(product);
  expect(await productDetailsPage.getProductNameLabel()).to.equal(product, 'Products page header label is wrong!');
});
