const { cartPage } = require('../../../pageobjects');
const { When } = require('cucumber');

When(/the user proceeds checkout/, async function () {
  return cartPage.clickButtonCheckout();
});
