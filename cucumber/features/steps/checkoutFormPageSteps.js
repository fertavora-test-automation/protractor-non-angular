const { checkoutFormPage } = require('../../../pageobjects');
const { When } = require('cucumber');

When(/the user fill the checkout form/, async function () {
  await checkoutFormPage.enterFirstName('John');
  await checkoutFormPage.enterLastName('Doe');
  await checkoutFormPage.enterPostalCode('1234');
  return checkoutFormPage.clickButtonContinue();
});
