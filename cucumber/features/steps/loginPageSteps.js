const { loginPage, productsPage } = require('../../../pageobjects');
const { Given } = require('cucumber');
const chai = require('chai');
const { expect } = chai;

Given(/the user is signed in/, async function () {
  await loginPage.goToLoginPage();
  await loginPage.enterUsername('standard_user');
  await loginPage.enterPassword('secret_sauce');
  await loginPage.clickLoginButton();
  expect(await productsPage.inventoryContainerIsDisplayed()).to.be.true;
});
