const { overviewPage } = require('../../../pageobjects');
const { When } = require('cucumber');

When(/the user confirms the checkout overview/, async function () {
  return overviewPage.clickButtonFinish();
});
