const { productDetailsPage } = require('../../../pageobjects');
const { When } = require('cucumber');
const chai = require('chai');
const { expect } = chai;

When(/the user add the product to the cart/, async function () {
  await productDetailsPage.clickButtonAddToCart();
  expect(await productDetailsPage.buttonGoToCart().getText()).to.equal('1');
  expect(await productDetailsPage.buttonRemoveFromCartIsDisplayed()).to.be.true;
});

When(/the user goes to the cart page/, async function () {
  await productDetailsPage.clickButtonGoToCart();
});
