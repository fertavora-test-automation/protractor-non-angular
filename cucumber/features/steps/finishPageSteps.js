const { finishPage } = require('../../../pageobjects');
const { When } = require('cucumber');
const chai = require('chai');
const { expect } = chai;

When(/the finish page is displayed/, async function () {
  expect(await finishPage.getFinishHeaderText()).to.equal('THANK YOU FOR YOUR ORDER', 'Finish page header is not correct!');
});
