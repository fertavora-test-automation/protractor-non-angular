Feature: Checkout
  As a customer
  I want to select and add a product to the cart
  so I can proceed checkout

  Scenario: User selects a product, adds to cart and proceeds checkout
    Given the user is signed in
    When the user selects product Sauce Labs Bolt T-Shirt
    And the user add the product to the cart
    And the user goes to the cart page
    And the user proceeds checkout
    And the user fill the checkout form
    And the user confirms the checkout overview
    Then the finish page is displayed

