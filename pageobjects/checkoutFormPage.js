const { ExpectedConditions } = require('protractor');

const checkoutFormPage = {
  inputFirstName: () => element(by.css('#first-name')),
  inputLastName: () => element(by.css('#last-name')),
  inputPostalCode: () => element(by.css('#postal-code')),
  buttonContinue: () => element(by.css('.btn_primary.cart_button')),

  enterFirstName: async firstname => {
    await browser.wait(ExpectedConditions.visibilityOf(checkoutFormPage.inputFirstName()));
    return checkoutFormPage.inputFirstName().sendKeys(firstname);
  },
  enterLastName: async lastname => {
    await browser.wait(ExpectedConditions.visibilityOf(checkoutFormPage.inputLastName()));
    return checkoutFormPage.inputLastName().sendKeys(lastname);
  },

  enterPostalCode: async postalCode => {
    await browser.wait(ExpectedConditions.visibilityOf(checkoutFormPage.inputPostalCode()));
    return checkoutFormPage.inputPostalCode().sendKeys(postalCode);
  },

  clickButtonContinue: async () => {
    await browser.wait(ExpectedConditions.elementToBeClickable(checkoutFormPage.buttonContinue()));
    return checkoutFormPage.buttonContinue().click();
  },
};

module.exports = checkoutFormPage;
