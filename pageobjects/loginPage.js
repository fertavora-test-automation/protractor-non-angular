const { ExpectedConditions } = require('protractor');

const loginPage = {
    inputUsername: () => element(By.css('#user-name')),
    inputPassword: () => element(By.css('#password')),
    buttonLogin: () => element(By.css('.btn_action')),

    enterUsername: async username => {
      await browser.wait(ExpectedConditions.visibilityOf(loginPage.inputUsername()));
      return loginPage.inputUsername().sendKeys(username);
    },
    enterPassword: async password => {
      await browser.wait(ExpectedConditions.visibilityOf(loginPage.inputPassword()));
      return loginPage.inputPassword().sendKeys(password);
    },

    clickLoginButton: async () => {
      await browser.wait(ExpectedConditions.elementToBeClickable(loginPage.buttonLogin()));
      return loginPage.buttonLogin().click();
    },

    goToLoginPage: async () => browser.get('/')
};

module.exports = loginPage;
