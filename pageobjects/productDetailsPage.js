const { ExpectedConditions } = require('protractor');

const productDetailsPage = {
  productNameLabel: () => element(by.css('.inventory_details_name')),
  buttonAddToCart: () => element(by.css('.btn_primary.btn_inventory')),
  buttonRemoveFromCart: () => element(by.css('.btn_secondary.btn_inventory')),
  buttonGoToCart: () => element(by.css('.shopping_cart_link')),

  getProductNameLabel: async () => {
    await browser.wait(ExpectedConditions.visibilityOf(productDetailsPage.productNameLabel()));
    return productDetailsPage.productNameLabel().getText();
  },
  clickButtonAddToCart: async () => {
    await browser.wait(ExpectedConditions.elementToBeClickable(productDetailsPage.buttonAddToCart()));
    return productDetailsPage.buttonAddToCart().click();
  },
  clickButtonRemoveFromCart: async () => {
    await browser.wait(ExpectedConditions.elementToBeClickable(productDetailsPage.buttonRemoveFromCart()));
    return productDetailsPage.buttonRemoveFromCart().click();
  },
  clickButtonGoToCart: async () => {
    await browser.wait(ExpectedConditions.elementToBeClickable(productDetailsPage.buttonGoToCart()));
    return productDetailsPage.buttonGoToCart().click();
  },

  buttonRemoveFromCartIsDisplayed: async () => browser.wait(ExpectedConditions.visibilityOf(productDetailsPage.buttonRemoveFromCart()))
};

module.exports = productDetailsPage;
