const { ExpectedConditions } = require('protractor');

const productsPage = {
  headerLabel: () => element(by.css('.header_label')),
  itemByLabel: label => element(by.xpath(`//div[contains(@class,"inventory_item_name")][text()="${label}"]`)),
  inventoryContainer: () => element(by.css('#inventory_container.inventory_container')),

  getHeaderLabel: () => productsPage.headerLabel().getText(),
  clickItemByLabel: async label => {
    await browser.wait(ExpectedConditions.elementToBeClickable(productsPage.itemByLabel(label)));
    return productsPage.itemByLabel(label).click();
  },

  inventoryContainerIsDisplayed: async () => browser.wait(ExpectedConditions.visibilityOf(productsPage.inventoryContainer()))
};

module.exports = productsPage;
