const { ExpectedConditions } = require('protractor');

const cartPage = {
  buttonCheckout: () => element(by.css('.btn_action.checkout_button')),

  clickButtonCheckout: async () => {
    await browser.wait(ExpectedConditions.elementToBeClickable(cartPage.buttonCheckout()));
    return cartPage.buttonCheckout().click();
  }
};

module.exports = cartPage;
