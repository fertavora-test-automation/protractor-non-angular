const finishPage = {
  finishHeader: () => element(by.css('.complete-header')),

  getFinishHeaderText: async () => {
    await browser.wait(ExpectedConditions.visibilityOf(finishPage.finishHeader()));
    return finishPage.finishHeader().getText();
  }
};

module.exports = finishPage;
