const { ExpectedConditions } = require('protractor');

const overviewPage = {
  buttonFinish: () => element(by.css('.btn_action.cart_button')),

  clickButtonFinish: async () => {
    await browser.wait(ExpectedConditions.elementToBeClickable(overviewPage.buttonFinish()));
    return overviewPage.buttonFinish().click();
  }
};

module.exports = overviewPage;
