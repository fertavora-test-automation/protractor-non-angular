exports.loginPage = require('./loginPage');
exports.productsPage = require('./productsPage');
exports.productDetailsPage = require('./productDetailsPage');
exports.cartPage = require('./cartPage');
exports.checkoutFormPage = require('./checkoutFormPage');
exports.overviewPage = require('./overviewPage');
exports.finishPage = require('./finishPage');
