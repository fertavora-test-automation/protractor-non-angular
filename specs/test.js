const { loginPage, productsPage, productDetailsPage } = require('../pageobjects');
const chai = require('chai');
const { expect } = chai;

describe('User selects a product, adds to cart and proceeds checkout', () => {
  it('Product is selected', async function () {
    await loginPage.goToLoginPage();
    await loginPage.enterUsername('standard_user');
    await loginPage.enterPassword('secret_sauce');
    await loginPage.clickLoginButton();
    await productsPage.clickItemByLabel('Sauce Labs Bolt T-Shirt');
    expect(await productDetailsPage.getProductNameLabel()).to.equal('Sauce Labs Bolt T-Shirt', 'Products page header label is wrong!');
  });

  it('Product is added to cart', async function() {
    await productDetailsPage.clickButtonAddToCart();
    await productDetailsPage.clickButtonGoToCart();
    return browser.sleep(5000);
  });
});
