exports.config = {
  framework: 'mocha',
  mochaOpts: {
    reporter: 'mochawesome',
    reporterOptions: {
      overwrite: false,
      quiet: true
    },
    slow: 60000,
    timeout: 180000,
    fullTrace: true
  },
  capabilities: {
    browserName: 'chrome',
    'goog:chromeOptions': {
      args: ['start-maximized']
    }
  },
  specs: [
    'specs/test.js'
  ],
  baseUrl: 'https://www.saucedemo.com',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  SELENIUM_PROMISE_MANAGER: false,
  onPrepare: () => browser.waitForAngularEnabled(false)
};
